package com.atlassian.plugins.confluence.bradbitt.markdown.impl;

import java.util.Map;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.webresource.api.assembler.PageBuilderService;
import org.springframework.beans.factory.annotation.Autowired;

public class HelloWorld implements Macro {
	
	private PageBuilderService pageBuilderService;

    @Autowired
    public helloworld(@ComponentImport PageBuilderService pageBuilderService) {
        this.pageBuilderService = pageBuilderService;
    }

	@Override
	public String execute(Map<String, String> arg0, String arg1, ConversionContext arg2) throws MacroExecutionException {
		pageBuilderService.assembler().resources()
			.requireWebResource("com.atlassian.plugins.confluence.bradbitt.markdown:markdown-resources");
		
		return "<h1>Hello World</h1>";
	}

	@Override
	public BodyType getBodyType() {
		return BodyType.NONE;
	}

	@Override
	public OutputType getOutputType() {
		return OutputType.BLOCK;
	}
}