package com.atlassian.plugins.confluence.bradbitt.markdown.api;

public interface MyPluginComponent
{
    String getName();
}