package ut.com.atlassian.plugins.confluence.bradbitt.markdown;

import org.junit.Test;
import com.atlassian.plugins.confluence.bradbitt.markdown.api.MyPluginComponent;
import com.atlassian.plugins.confluence.bradbitt.markdown.impl.MyPluginComponentImpl;

import static org.junit.Assert.assertEquals;

public class MyComponentUnitTest
{
    @Test
    public void testMyName()
    {
        MyPluginComponent component = new MyPluginComponentImpl(null);
        assertEquals("names do not match!", "myComponent",component.getName());
    }
}