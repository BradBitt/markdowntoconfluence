package com.atlassian.plugins.confluence.bradbitt.markdown.util;

import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;

import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class Validator {
	
	public static final String FILE_EXTENSION = "md";
	private static final Logger log = LoggerFactory.getLogger(Validator.class);
	
	/**
	 * This function takes in an input stream and checks to see if its a 
	 * valid input stream.
	 * @param stream
	 * @return boolean
	 */
	public static boolean checkValidStream(InputStream stream) {	
		return stream != null;
	}
	
	/**
	 * This function takes in a port and a hostname and checks to see
	 * if both the hostname and port are valid and neither is empty.
	 * @param hostname
	 * @param port
	 * @return boolean
	 */
	public static boolean checkValidproxy(String hostname, String port) {

		// If either of them is empty then fail.
		if (port.isEmpty()) {
			log.warn("Can't include a hostname without a port!");
			return false;
		}
		
		if (hostname.isEmpty()) {
			log.warn("Can't include a port without a hostname!");
			return false;
		}
		
		// Checks to see if the port contains doesn't only contain numbers.
		if (!port.matches("[0-9]+")) {
			log.warn("Port should only contain numbers!");
			return false;
		}
		
		return true;
	}
	
	/**
	 * This function checks to see if the URL is valid.
	 * @param url
	 * @return boolean
	 * @throws MalformedURLException 
	 * @throws URISyntaxException 
	 */
	public static boolean checkValidUrl(String urlString) {
		
		// Checks to see if the URL can be converted to URI without failing.
		try {
			URL url = new URL(urlString);
			url.toURI();
			
			if (checkUrlIsCorrectFile(urlString)) {
				return true;
			} else {
				return false;
			}
			
		} catch (URISyntaxException | MalformedURLException e) {
			log.error("The URL provided is not valid!", e);
			return false;
		} 
	}
	
	/**
	 * This function checks if the URL given points to a markdown file.
	 * The developer could change this function so other files and not just
	 * markdown files could be used.
	 * @param url
	 * @return boolean
	 */
	private static boolean checkUrlIsCorrectFile(String url) {
		if (FilenameUtils.getExtension(url).equals(FILE_EXTENSION)) {
			return true;
		}
		log.warn("The File Extenstion in the url used does not match: " + FILE_EXTENSION);
		return false;
	}
	
	private Validator() {}
}