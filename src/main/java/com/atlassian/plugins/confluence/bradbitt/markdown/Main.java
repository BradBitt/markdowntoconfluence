package com.atlassian.plugins.confluence.bradbitt.markdown;

import java.io.InputStream;
import java.util.Map;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.plugins.confluence.bradbitt.markdown.converter.Converter;
import com.atlassian.webresource.api.assembler.PageBuilderService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

@Scanned
public class Main implements Macro {
	
	private static final String PROJECT_NAME = "com.atlassian.plugins.confluence.bradbitt.markdown";
	private static final String WEB_RESOURCE_NAME = "MarkdownToConfluence-resources";
	
	private static final Logger log = LoggerFactory.getLogger(Main.class);
	private PageBuilderService pageBuilderService;

    @Autowired
    public Main(@ComponentImport PageBuilderService pageBuilderService) {
        this.pageBuilderService = pageBuilderService;
    }

	@Override
	public String execute(Map<String, String> parameters, String body, ConversionContext context) throws MacroExecutionException {
		// This call activates the required web resources. This causes the live preview to work also.
		pageBuilderService.assembler().resources()
			.requireWebResource(PROJECT_NAME + ":" + WEB_RESOURCE_NAME);
		
		// Gets the input stream from the URL if it exists.
		InputStream mdFile = FileRetriever.getStream(parameters);
		if (mdFile == null) {
			log.error("Unable to download contents!");
			return "FAIL - Unable to download contents!";
		}
		
		// Converts the contents of the input stream to html.
		String html = Converter.markdownToHTML(mdFile);
		if (html.isEmpty()) {
			log.error("Unable to convert the markdown content to HTML!");
			return "FAIL - Unable to convert the markdown content to HTML!";
		}
		
		// Converts the html to confluence supported text.

		return html;
	}

	@Override
	public BodyType getBodyType() {
		return BodyType.NONE;
	}

	@Override
	public OutputType getOutputType() {
		return OutputType.BLOCK;
	}
}