package com.atlassian.plugins.confluence.bradbitt.markdown.converter;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vladsch.flexmark.html.HtmlRenderer;
import com.vladsch.flexmark.parser.Parser;
import com.vladsch.flexmark.util.ast.Node;
import com.vladsch.flexmark.util.data.MutableDataSet;

public final class Converter {
	
	private static final Logger log = LoggerFactory.getLogger(Converter.class);
	
	/**
	 * This function takes in an input stream.
	 * It then converts this input stream of markdown content to
	 * html content. It then returns this as a string.
	 * @param stream
	 * @return String
	 */
	public static String markdownToHTML(InputStream stream) {
		
		MutableDataSet options = new MutableDataSet();
		Parser parser = Parser.builder(options).build();
        HtmlRenderer renderer = HtmlRenderer.builder(options).build();
        
        Node document = null;
        try {
        	document = parser.parseReader(new InputStreamReader(stream));
		} catch (IOException e) {
			log.error("Unable to convert Markdown to HTML!", e);
			return "";
		}
        
        return renderer.render(document);
	}
	
	private Converter() {}
}