package com.atlassian.plugins.confluence.bradbitt.markdown;

import java.io.IOException;
import java.io.InputStream;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URL;
import java.net.URLConnection;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.plugins.confluence.bradbitt.markdown.util.Validator;

public final class FileRetriever {
	
	private static final Logger log = LoggerFactory.getLogger(FileRetriever.class);
	private static final String URL_KEY = "URL";
	private static final String HOSTNAME_KEY = "Hostname";
	private static final String PORT_KEY = "Port";
	private static final String ERROR_MESSAGE = 
			"Was unable to connect to the URL. Was unable to get the inputstream from URL";
	
	/**
	 * This function takes in the macro parameters and returns the
	 * input stream of the file in the URL.
	 * @param parameters
	 * @return InputStream
	 */
	public static InputStream getStream(Map<String, String> parameters) {
		String hostname = parameters.get(HOSTNAME_KEY);
		String port = parameters.get(PORT_KEY);
		String url = parameters.get(URL_KEY);
		
		// Checks to see if a hostname and a port was provided
		// Also checks to see if the url is valid and points to a markdown file.
		InputStream mdFile = null;
		if(Validator.checkValidproxy(hostname, port) && Validator.checkValidUrl(url)) {
			mdFile = FileRetriever.getURLStream(parameters.get(URL_KEY), hostname, Integer.parseInt(port));
		} else {
			mdFile = FileRetriever.getURLStream(parameters.get(URL_KEY));
		}
		
		return mdFile;
	}
	
	/**
	 * This function is used to return an input stream of the url provided.
	 * @param url
	 * @return InputStream
	 * @throws IOException
	 */
	private static InputStream getURLStream(String url) {
		
		try {
			// Connects to the site.
			URLConnection con = new URL(url).openConnection();
			con.connect();
			
			InputStream stream = con.getInputStream();
			if (Validator.checkValidStream(con.getInputStream())) {
				return stream;
			} else {
				return null;
			}
		} catch (IOException e) {
			log.error(ERROR_MESSAGE, e);
			return null;
		}
	}
	
	/**
	 * This function is used to return an input stream of the url provided. It uses the proxy
	 * provided to get the input stream.
	 * @param url
	 * @param hostname
	 * @param port
	 * @return InputStream
	 * @throws IOException
	 */
	private static InputStream getURLStream(String url, String hostname, int port) {
		
		try {
			// Uses a proxy to access the site.
			Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(hostname, port));
			URLConnection con = new URL(url).openConnection(proxy);
			con.connect();
		
			InputStream stream = con.getInputStream();
			if (Validator.checkValidStream(con.getInputStream())) {
				return stream;
			} else {
				return null;
			}
		} catch (IOException e) {
			log.error(ERROR_MESSAGE, e);
			return null;
		}
	}
	
	private FileRetriever() {}
}