# ConfluencePlugin

Download the SDK from [here](https://marketplace.atlassian.com/apps/1210950/atlassian-plugin-sdk-windows?hosting=server&tab=overview).

run: atlas-version.bat to set up the JDK.
Used the atlas-create-confluence-plugin.bat file to create a skeleton. Had to add: '-Dallow.google.tracking=false' to the maven command line executed in the .bat file as it wouldnt run without this extra param.

## How to run the SDK

Open up a cmd and go to the root directory of this plugin. This plugin should be located inside the bin folder of the SDK. in the cmd, run 'atlas-run'. [This](https://developer.atlassian.com/server/framework/atlassian-sdk/create-a-helloworld-plugin-project/) explains how to to run a plugin in the SDK. The admin username and password is 'admin' and 'admin'. Need to add the proxy to the maven used in the SDK. Also have to add this maven command in order to ignore certain certificates: '-Dmaven.wagon.http.ssl.insecure=true'
Once running, was able to view the confluence site through this localhost and port: http://localhost:1990/confluence. Use ctrl+z to close the confluence site safely.
Added the following to the .m2/settings.xml file, This allows maven to access repositories that have problems with certificates and also avoids google tracking:

	<profiles>
		<profile>
			<id>bradbitt</id>
			<properties>
				<maven.wagon.http.ssl.insecure>true</maven.wagon.http.ssl.insecure>
				<allow.google.tracking>false</allow.google.tracking>
			</properties>
		</profile>
	</profiles>
	<activeProfiles>
        <activeProfile>bradbitt</activeProfile>
    </activeProfiles>

Use: 'atlas-package', in order to quick compile and build the changes you have made, otherwise it wont run when running the 'atlas-run' file.

## How to Log issues in the plugin
Confluence uses SLF4J as a logging client. The current practice in Confluence is to create a static final logger called log (lower-case) in each class that needs to perform logging. Use the following code:

	private static final Logger log = LoggerFactory.getLogger(TheCurrentClass.class);

## Other Notes

- ETAS-DEV Confluence - 6.14.3
- SDK Confluence - 6.14.0
- If you get this following error when running 'atlas-package': 'The default package '.' is not permitted by the Import-Package syntax.' This is because you need to delete the following folders in the '/target/' folder.
	- '/target/classes/'
	- '/target/obr/'
	- '/target/container/tomcat9x/cargo-confluence-home'
- Using [this](https://github.com/vsch/flexmark-java) opensource project to convert Markdown to HTML.
- When adding external jar dependencies, make sure to include the scope as 'provided'.
- Names such as Macro names, can't have any spaces in them or start with a capital letter, the '-' character works as a space in names.

## Here are the SDK commands:

* atlas-run   -- installs this plugin into the product and starts it on localhost
* atlas-debug -- same as atlas-run, but allows a debugger to attach at port 5005
* atlas-help  -- prints description for all commands in the SDK

Full documentation is always available [here](https://developer.atlassian.com/display/DOCS/Introduction+to+the+Atlassian+Plugin+SDK)